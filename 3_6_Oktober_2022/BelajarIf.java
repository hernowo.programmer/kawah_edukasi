public class BelajarIf{

    static int angka1 = 30;
    static int angka2 = 30;
    static int angka3 = 40;
    static int angka4 = 50;
    static int angka5 = 5;

    //If with one branch
    static void belajarIf1(){
        if(angka1 > 20){
            System.out.println("Angka1 lebih besar dari 20");
        }
    }

    //If with two branch
    static void belajarIf2(){
        if(angka2 > 20){
            System.out.println("Angka2 lebih besar dari 20");
        }else{
            System.out.println("Angka2 lebih kecil dari 20");
        }
    }

    //if with three or more branch
    static void belajarIf3(){
        if(angka3 > 20){ 
            System.out.println("Angka3 lebih besar dari 20");
        }else if(angka3 < 20){
            System.out.println("Angka3 lebih kecil dari 20");
        }else{
            System.out.println("Angka3 sama dengan 20");
        }
    }

    //Tenary Operation
    static void belajarIf4(){
        String Penampung = (angka4>20) ? "Angka4 Lebih Besar Dari 20" : "Angka4 Tidak Lebih Besar dari 20";
        System.out.println(Penampung);
    }

    //Contoh Belajar Switch
    static void belajarIf5(){
        switch(angka5){
            case 1:
                System.out.println("Senin");
                break;
            case 2:
                System.out.println("Selasa");
                break;
            case 3:
                System.out.println("Rabu");
                break;
            case 4:
                System.out.println("Kamis");
                break;
            case 5:
                System.out.println("Jum'at");
                break;
            case 6:
                System.out.println("Sabtu");
            default:
                System.out.println("Hari tidak ditemukan");
        }
    }

    public static void main(String[] args){
        
        belajarIf1();
        belajarIf2();
        belajarIf3();
        belajarIf4();
        belajarIf5();
    }

}