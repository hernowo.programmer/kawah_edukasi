import java.util.Scanner; //import library java.util.Scanner

public class BelajarInput{ //make main class
    public static void main(String[] args){ //make main method
        Scanner scanner = new Scanner(System.in); //make scanner objek that pas System.in parameter
        System.out.print("Masukkan Angka Tertentu: "); //invoker method print
        int angkaDariUser = scanner.nextInt(); //access objek scanner and invoke method nextInt put the value to int angkaDariUser

        System.out.println("Angkanya adalah " + angkaDariUser); //invoke method pritln
        scanner.close(); //access objek scanner and invoke method close to close Scanner
    }
}